/**
 * Created by chandru on 29/6/18.
 */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppURLService} from "./app-url.service";

@Injectable()
export class AssetService {
    constructor(private http: HttpClient, public configService: AppURLService) {
    }
    
    saveAssetDetails(assetData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'assetDetails', assetData);
    }
    deleteAssetDetailsByMongodbId(assetMongoDbId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssetById/' + assetMongoDbId);
    }

    getAssetPaginationCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'getAllAssetCount');
    }

    getAssetsBasedOnRange(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'assetsByLimit?skip=' + skip + '&limit=' + limit);
    }
    getAssetDetailsByMongodbId(assetId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'asset/' + assetId);
    }
    updateAssetsById(assetId, assetDetails) {
        return this.http.put(this.configService.appConfig.appBaseUrl + 'asset/' + assetId, assetDetails);
    }

  

}


