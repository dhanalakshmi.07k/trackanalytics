import {Injectable} from '@angular/core';

@Injectable()
export class ConfigService {
    public assetDropDownConfig = {
        "Duty":["GSU","Transmission","Distribution","Inductance","Rectifier"," Arc Furnace","Industrial","Other","Unknown"],
        "typeOfPaper":[],
        "OilTypeMainTank":[],
        "Specification":[]
    };

    constructor() {
    }

}
