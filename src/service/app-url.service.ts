import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppURLService {
  public appConfig = {
    appBaseUrl: 'http://localhost:5000/'
};
constructor() {
}
}
