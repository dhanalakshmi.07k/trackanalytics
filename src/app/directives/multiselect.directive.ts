import {Directive, Input, Output, ElementRef, OnInit, OnChanges, EventEmitter} from '@angular/core';

declare let $: any;

@Directive({
    selector: '[appMultiSelect]'
})
export class MultiSelectDirective implements OnInit {
    value;
    valueObj;
    @Output() getValues = new EventEmitter();

    constructor(private ele: ElementRef) {

    }

    nativeEle = this.ele.nativeElement;

    ngOnInit(): void {
        console.log(this.ele.nativeElement)
        // $(this.nativeEle).find('.multiSelect .list li')
        const _this = this;
        $('body').on('click', '.multiSelect .list li', function (e) {
            e.stopPropagation();
            const that = this;
            const listHeader = $(this).parents().eq(1).find('.headTitle');
            let selectedVal = '';
            setTimeout(() => {
                const selectionLen = $(that).parent().find('input[type=\'checkbox\']:checked').length;
                if (selectionLen === 0) {
                    listHeader.text('');
                    listHeader.val('');
                    _this.value = '';
                    _this.valueObj = [];
                    _this.getValues.emit(_this.valueObj);
                } else {
                    $(that).parent().find('input[type=\'checkbox\']:checked').each(function () {
                        selectedVal = selectedVal + $(this).parents().eq(1).find('.listName').text().trim() + ', ';
                    });
                    const headerVal = selectedVal.trim().slice(0, selectedVal.length - 2);
                    listHeader.text(headerVal);
                    listHeader.val(headerVal);
                    _this.value = headerVal;
                    _this.valueObj = headerVal.split(',');
                    _this.getValues.emit(_this.valueObj);
                }
            }, 150);
        });
    }
}
