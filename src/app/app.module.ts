import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {SettingsComponent} from './settings/settings.component';
import {FormsModule} from "@angular/forms";
import {MultiSelectDirective} from "./directives/multiselect.directive";
import {OwlDateTimeModule} from "ng-pick-datetime";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {OwlMomentDateTimeModule} from "ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time.module";
import {MomentModule} from "ngx-moment";
import {InputFileModule} from "ngx-input-file";
import { TableComponent } from './table/table.component';
import {FilterPipe} from "./directives/filter.pipe";
import {SortDirective} from './sort-directive.directive';
import { LoginComponent } from './login/login.component';
import { AppMainComponent } from './app-main/app-main.component';
import { SignupComponent } from './signup/signup.component';
import { AssetDetailsComponent } from './asset-details/asset-details.component';

// service
import {ConfigService} from "../service/config.service";
import {AssetService} from "../service/asset.service";
import {AppURLService} from "../service/app-url.service";

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        NotificationsComponent,
        SettingsComponent,
        MultiSelectDirective,
        TableComponent,
        FilterPipe,
        SortDirective,
        LoginComponent,
        AppMainComponent,
        SignupComponent,
        AssetDetailsComponent,

    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FormsModule,
        OwlDateTimeModule,
        OwlMomentDateTimeModule,
        MomentModule,
        InputFileModule.forRoot({})
    ],
    providers: [ 
        ConfigService,
        AssetService,
        AppURLService
    
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
