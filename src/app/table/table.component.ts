import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

    constructor() {
    }

    filterText = '';
    students = [
        {
            name: 'John Cena',
            email: 'John@gmail.com',
            collage: 'Bangalore University',
            qualification: 'B.tech',
            place: 'Bangalore'
        },
        {
            name: 'John Mathew',
            email: 'John@gmail.com',
            collage: 'Bangalore University',
            qualification: 'C.tech',
            place: 'Bangalore'
        },
        {
            name: 'John Mathew',
            email: 'John@gmail.com',
            collage: 'Bangalore University',
            qualification: 'D.tech',
            place: 'Mysore'
        },
        {
            name: 'John Ibrahim',
            email: 'mathew@gmail.com',
            collage: 'Bangalore University',
            qualification: 'E.tech',
            place: 'Bangalore'
        },
        {
            name: 'John Mathew',
            email: 'John@gmail.com',
            collage: 'Bangalore University',
            qualification: 'F.tech',
            place: 'Delhi'
        },
        {
            name: 'John Mathew',
            email: 'mathew@gmail.com',
            collage: 'Bangalore University',
            qualification: 'MCA',
            place: 'Bangalore'
        },
        {
            name: 'John Mathew',
            email: 'John@gmail.com',
            collage: 'Bangalore University',
            qualification: 'BCA',
            place: 'Goa'
        },
        {
            name: 'John Mathew',
            email: 'John@gmail.com',
            collage: 'Bangalore University',
            qualification: 'B.com',
            place: 'Bangalore'
        }
    ];

    ngOnInit() {
    }

}
