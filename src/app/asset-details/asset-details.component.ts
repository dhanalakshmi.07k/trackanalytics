import { Component, OnInit } from '@angular/core';
import {ConfigService} from "../../service/config.service";

@Component({
  selector: 'app-asset-details',
  templateUrl: './asset-details.component.html',
  styleUrls: ['./asset-details.component.css']
})
export class AssetDetailsComponent implements OnInit {

  constructor(public appDropDownConfig:ConfigService) {
     
  }
  public assetDetails:any = {
    // assetId: '',
    // assetName: '',
    //   middleName: '',
    //   lastName: '',
    //   dob: '',
    //   age: null,
    //   professional: '',
    //   languages: [],
    //   gender: '',
    //   completed: [],
    //   comment: '',
    //   upload: []
  };
//   public assetDetails={};
  filterText = '';
  students = [
      {
          assetId: 'John Cena',
          assetName: 'John@gmail.com'
      },
      {
        assetId: 'John Mathew',
        assetName: 'John@gmail.com'
      }
  ];
 public dutyList = this.appDropDownConfig.assetDropDownConfig.Duty;
  saveAssetDetails(){
    console.log(this.appDropDownConfig)
    console.log(this.assetDetails)
  }

  // settinsg tab


uploaded = {
    img: []
};
getLanguagesKnown(val) {
    // this.assetDetails.languages = val;
}

pushOrPullIntoArray(val, array) {
    if (array.includes(val)) {
        const index = array.indexOf(val);
        array.splice(index, 1);
    } else {
        array.push(val);
    }
}
setDOB(val) {
    console.log(val['value']);
}

getFormData(form) {
    // console.log(form);
    console.log(this.assetDetails);
}

getImageUpload(val) {
    console.log(val);
    this.uploaded.img = val;
    console.log(this.uploaded);
}



deleteImg() {
    this.uploaded = {
        img: []
    };
} 


ngOnInit() {
}
}
