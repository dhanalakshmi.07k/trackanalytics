import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {SettingsComponent} from "./settings/settings.component";
import {TableComponent} from "./table/table.component";
import {LoginComponent} from "./login/login.component";
import {AppMainComponent} from "./app-main/app-main.component";
import {SignupComponent} from "./signup/signup.component";
import {AssetDetailsComponent} from "./asset-details/asset-details.component";



const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
        pathMatch: 'full'
    },
    {
        path: 'new',
        component: SignupComponent,
        pathMatch: 'full'
    },
    {
        path: 'app',
        component: AppMainComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent,
                pathMatch: 'full'
            },
            {
                path: 'settings',
                component: SettingsComponent,
                pathMatch: 'full'
            },
            {
                path: 'table',
                component: TableComponent,
                pathMatch: 'full'
            },
            {
                path: 'assetDetails',
                component: AssetDetailsComponent,
                pathMatch: 'full'
            }        ]
    },
    {
        path: '**',
        redirectTo: '/login',
        pathMatch: 'full'
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
